<?php

/**
 * @file
 *
 * file for template
 */
function maya_preprocess_html(&$vars) {
  // Add conditional stylesheets for ubercart
  if (module_exists('uc_store')) {
    drupal_add_css(path_to_theme() . '/css/ubercart.css', array('group' => CSS_THEME, 'preprocess' => TRUE));
  }
}

function maya_breadcrumb(&$vars) {
  $breadcrumb = $vars['breadcrumb'];

  if ($breadcrumb && count($breadcrumb) > 1) {
    return '<div id="breadcrumb">' . implode(' › ', $breadcrumb) . '</div>';
  }
}

function maya_tablesort_indicator($vars) {
  global $theme_path;

  if ($vars['style'] == "asc") {
    return theme('image', array('path' => $theme_path . '/images/arrow-small-white-down.png', 'alt' => t('sort ascending'), 'title' => t('sort ascending')));
  }
  else {
    return theme('image', array('path' => $theme_path . '/images/arrow-small-white-up.png', 'alt' => t('sort descending'), 'title' => t('sort descending')));
  }
}

function maya_preprocess_node(&$vars) {
  global $user;
  $node = $vars['node'];
  $account = user_load($node->revision_uid);

  $vars['submitted'] = theme('user_picture', array(
        'account' => $account,
        'user_picture_style' => 'user_node'
      )) . '<div class="node-user">' . l(format_username($account), 'user/' . $account->uid) . '</div><div class="node-date">' . format_date($node->changed, 'custom', 'd.m.Y') . '</div>';

  if ($vars['view_mode'] == 'full') {
    if ($vars['type'] == 'file_date' && isset($vars['field_attachment']) && count($vars['field_attachment']) == 1 && isset($vars['field_attachment'][0])) {
      $url = file_create_url($vars['field_attachment'][0]['uri']);

      if ($url !== FALSE) {
        drupal_goto($url);
      }
    }

    if ($vars['type'] == 'link' && isset($vars['field_link'][0])) {
      $url = str_replace('[LOGINNAME]', $user->name, $vars['field_link'][0]['original_url']);
      $url = str_replace('%5BLOGINNAME%5D', $user->name, $url);

      if ($url !== FALSE) {
        drupal_goto($url);
      }
    }
  }
}

function maya_preprocess_comment(&$vars) {
  $vars['submitted'] = '<div class="comment-user">' . $vars['author'] . '</div>';
  $vars['created'] = format_date($vars['comment']->created, 'custom', 'd.m.Y');
  ;
}

function maya_pager(&$vars) {
  $element = $vars['element'];
  $parameters = $vars['parameters'];
  $quantity = 5; // $vars['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.
  // Prepare for generation loop.
  $page = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $page = $page + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($page <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $page);
    $page = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => '«', 'element' => $element, 'parameters' => $parameters));
  //$li_previous = theme('pager_previous', array('text' => '‹', 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  //$li_next = theme('pager_next', array('text' => '›', 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => '»', 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
    if ($li_first && $pager_max > $quantity) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }
    // When there is more than one page, create the pager list.
    if ($page != $pager_max) {
      // Now generate the actual pager piece.
      for (; $page <= $pager_last && $page <= $pager_max; $page++) {
        if ($page < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $page, 'element' => $element, 'interval' => ($pager_current - $page), 'parameters' => $parameters)),
          );
        }
        if ($page == $pager_current) {
          $items[] = array(
            'class' => array('pager-current'),
            'data' => $page,
          );
        }
        if ($page > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $page, 'element' => $element, 'interval' => ($page - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
    }
    // End generation.
    if ($li_last && $pager_max > $quantity) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }
    return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . theme('item_list', array(
          'items' => $items,
          'attributes' => array('class' => array('pager')),
    ));
  }
}

/* function maya_text_resize_block() {
  drupal_add_library('system', 'jquery.cookie');
  drupal_add_js("var text_resize_scope = " . drupal_json_encode(variable_get('text_resize_scope', 'main')) . ";
  var text_resize_minimum = " . drupal_json_encode(variable_get('text_resize_minimum', '12')) . ";
  var text_resize_maximum = " . drupal_json_encode(variable_get('text_resize_maximum', '25')) . ";
  var text_resize_line_height_allow = " . drupal_json_encode(variable_get('text_resize_line_height_allow', FALSE)) . ";
  var text_resize_line_height_min = " . drupal_json_encode(variable_get('text_resize_line_height_min', 16)) . ";
  var text_resize_line_height_max = " . drupal_json_encode(variable_get('text_resize_line_height_max', 36)) . ";", 'inline');
  drupal_add_js(drupal_get_path('module', 'text_resize') . '/text_resize.js', 'file');

  if (variable_get('text_resize_reset_button', FALSE) == TRUE) {
  $output = '<ul class="text-resize">
  <li class="text-resize-decrease"><a href="javascript:;" class="changer" id="text_resize_decrease">A<sup>-</sup></a></li>
  <li class="text-resize-reset"><a href="javascript:;" class="changer" id="text_resize_reset">A</a></li>
  <li class="text-resize-increase"><a href="javascript:;" class="changer" id="text_resize_increase"><sup>+</sup>A</a></li>
  </ul>';
  }
  else {
  $output = t('<a href="javascript:;" class="changer" id="text_resize_decrease"><sup>-</sup>A</a> <a href="javascript:;" class="changer" id="text_resize_increase"><sup>+</sup>A</a><div id="text_resize_clear"></div>');
  }
  return $output;
  } */

function maya_superfish($vars) {
  global $user, $language;

  $mid = $vars['id'];
  $menu_name = $vars['menu_name'];
  $mlid = $vars['mlid'];
  $sfsettings = $vars['sfsettings'];

  $cache_key = 'links:' . $menu_name . ':all-cid:' . $vars['mlid'] . ':' . $user->uid . ':' . $language->language;
  $cache = cache_get($cache_key, 'cache_menu');
  if ($cache && isset($cache->data)) {
    $cache = $cache->data;
  }
  else {
    // Start caching the menu.
    $cache = array();
    $cache['menu'] = menu_tree_all_data($menu_name);

    // For custom $menus and menus built all the way from the top-level we
    // don't need to "create" the specific sub-menu and we need to get the title
    // from the $menu_name since there is no "parent item" array.
    // Create the specific menu if we have a mlid.
    if (!empty($mlid)) {
      // Load the parent menu item.
      $item = menu_link_load($mlid);
      $cache['title'] = check_plain($item['title']);
      $cache['parent_depth'] = $item['depth'];
      // Narrow down the full menu to the specific sub-tree we need.
      for ($p = 1; $p < 10; $p++) {
        if ($sub_mlid = $item["p$p"]) {
          $subitem = menu_link_load($sub_mlid);
          $key = (50000 + $subitem['weight']) . ' ' . $subitem['title'] . ' ' . $subitem['mlid'];
          $cache['menu'] = (isset($cache['menu'][$key]['below'])) ? $cache['menu'][$key]['below'] : $cache['menu'];
        }
      }
    }
    else {
      $result = db_query("SELECT title FROM {menu_custom} WHERE menu_name = :a", array(':a' => $menu_name))->fetchField();
      $cache['title'] = check_plain($result);
    }
    cache_set($cache_key, $cache, 'cache_menu');
    $cache = cache_get($cache_key, 'cache_menu')->data;
  }

  $output['content'] = '';
  $output['subject'] = $cache['title'];
  if ($cache['menu']) {
    // Set the total menu depth counting from this parent if we need it.
    $depth = $sfsettings['depth'];
    $depth = ($sfsettings['depth'] > 0 && isset($cache['parent_depth'])) ? $cache['parent_depth'] + $depth : $depth;

    $var = array(
      'id' => $mid,
      'menu' => $cache['menu'],
      'depth' => $depth,
      'trail' => superfish_build_page_trail(menu_tree_page_data($menu_name)),
      'sfsettings' => $sfsettings
    );
    if ($menu_tree = theme('superfish_build', $var)) {
      if ($menu_tree['content']) {
        // Add custom HTML codes around the main menu.
        if ($sfsettings['wrapmul'] && strpos($sfsettings['wrapmul'], ',') !== FALSE) {
          $wmul = explode(',', $sfsettings['wrapmul']);
          // In case you just wanted to add something after the element.
          if (drupal_substr($sfsettings['wrapmul'], 0) == ',') {
            array_unshift($wmul, '');
          }
        }
        else {
          $wmul = array();
        }
        $output['content'] = isset($wmul[0]) ? $wmul[0] : '';
        $output['content'] .= '<ul id="superfish-' . $mid . '"';
        $output['content'] .= ' class="sf-menu ' . $menu_name . ' sf-' . $sfsettings['type'] . ' sf-style-' . $sfsettings['style'];
        $output['content'] .= ($sfsettings['itemcounter']) ? ' sf-total-items-' . $menu_tree['total_children'] : '';
        $output['content'] .= ($sfsettings['itemcounter']) ? ' sf-parent-items-' . $menu_tree['parent_children'] : '';
        $output['content'] .= ($sfsettings['itemcounter']) ? ' sf-single-items-' . $menu_tree['single_children'] : '';
        $output['content'] .= ($sfsettings['ulclass']) ? ' ' . $sfsettings['ulclass'] : '';
        $output['content'] .= ($language->direction == 1) ? ' rtl' : '';
        $output['content'] .= '">' . $menu_tree['content'] . '</ul>';
        $output['content'] .= isset($wmul[1]) ? $wmul[1] : '';
      }
    }
  }
  return $output;
}

/**
 * Helper function that builds the nested lists of a Superfish menu.
 */
function maya_superfish_build($vars) {
  $output = array('content' => '');
  $id = $vars['id'];
  $menu = $vars['menu'];
  $depth = $vars['depth'];
  $trail = $vars['trail'];
  // Keep $sfsettings untouched as we need to pass it to the child menus.
  $settings = $sfsettings = $vars['sfsettings'];
  $megamenu = $settings['megamenu'];
  $total_children = $parent_children = $single_children = 0;
  $counter = 1;

  // Reckon the total number of available menu items.
  foreach ($menu as $menu_item) {
    if (!isset($menu_item['link']['hidden']) || $menu_item['link']['hidden'] == 0) {
      $total_children++;
    }
  }

  foreach ($menu as $menu_item) {

    $show_children = $megamenu_wrapper = $megamenu_column = $megamenu_content = FALSE;
    $item_class = $link_options = $link_class = array();
    $mlid = $menu_item['link']['mlid'];

    if (!isset($menu_item['link']['hidden']) || $menu_item['link']['hidden'] == 0) {
      $item_class[] = ($trail && in_array($mlid, $trail)) ? 'active-trail' : '';

      // Add helper classes to the menu items and hyperlinks.
      $settings['firstlast'] = ($settings['dfirstlast'] == 1 && $total_children == 1) ? 0 : $settings['firstlast'];
      $item_class[] = ($settings['firstlast'] == 1) ? (($counter == 1) ? 'first' : (($counter == $total_children) ? 'last' : 'middle')) : '';
      $settings['zebra'] = ($settings['dzebra'] == 1 && $total_children == 1) ? 0 : $settings['zebra'];
      $item_class[] = ($settings['zebra'] == 1) ? (($counter % 2) ? 'odd' : 'even') : '';
      $item_class[] = ($settings['itemcount'] == 1) ? 'sf-item-' . $counter : '';
      $item_class[] = ($settings['itemdepth'] == 1) ? 'sf-depth-' . $menu_item['link']['depth'] : '';
      $link_class[] = ($settings['itemdepth'] == 1) ? 'sf-depth-' . $menu_item['link']['depth'] : '';
      $item_class[] = ($settings['liclass']) ? $settings['liclass'] : '';
      if (strpos($settings['hlclass'], ' ')) {
        $l = explode(' ', $settings['hlclass']);
        foreach ($l as $c) {
          $link_class[] = $c;
        }
      }
      else {
        $link_class[] = $settings['hlclass'];
      }
      $counter++;

      // Add hyperlinks description (title) to their text.
      $show_linkdescription = ($settings['linkdescription'] == 1 && !empty($menu_item['link']['localized_options']['attributes']['title'])) ? TRUE : FALSE;
      if ($show_linkdescription) {
        if (!empty($settings['hldmenus'])) {
          $show_linkdescription = (is_array($settings['hldmenus'])) ? ((in_array($mlid, $settings['hldmenus'])) ? TRUE : FALSE) : (($mlid == $settings['hldmenus']) ? TRUE : FALSE);
        }
        if (!empty($settings['hldexclude'])) {
          $show_linkdescription = (is_array($settings['hldexclude'])) ? ((in_array($mlid, $settings['hldexclude'])) ? FALSE : $show_linkdescription) : (($settings['hldexclude'] == $mlid) ? FALSE : $show_linkdescription);
        }
        if ($show_linkdescription) {
          $menu_item['link']['title'] .= '<span class="sf-description">';
          $menu_item['link']['title'] .= (!empty($menu_item['link']['localized_options']['attributes']['title'])) ? $menu_item['link']['localized_options']['attributes']['title'] : array();
          $menu_item['link']['title'] .= '</span>';
          $link_options['html'] = TRUE;
        }
      }

      // Add custom HTML codes around the menu items.
      if ($sfsettings['wrapul'] && strpos($sfsettings['wrapul'], ',') !== FALSE) {
        $wul = explode(',', $sfsettings['wrapul']);
        // In case you just wanted to add something after the element.
        if (drupal_substr($sfsettings['wrapul'], 0) == ',') {
          array_unshift($wul, '');
        }
      }
      else {
        $wul = array();
      }

      // Add custom HTML codes around the hyperlinks.
      if ($settings['wraphl'] && strpos($settings['wraphl'], ',') !== FALSE) {
        $whl = explode(',', $settings['wraphl']);
        // The same as above
        if (drupal_substr($settings['wraphl'], 0) == ',') {
          array_unshift($whl, '');
        }
      }
      else {
        $whl = array();
      }

      // Add custom HTML codes around the hyperlinks text.
      if ($settings['wraphlt'] && strpos($settings['wraphlt'], ',') !== FALSE) {
        $whlt = explode(',', $settings['wraphlt']);
        // The same as above
        if (drupal_substr($settings['wraphlt'], 0) == ',') {
          array_unshift($whlt, '');
        }
        $menu_item['link']['title'] = $whlt[0] . check_plain($menu_item['link']['title']) . $whlt[1];
        $link_options['html'] = TRUE;
      }


      if (!empty($menu_item['link']['has_children']) && !empty($menu_item['below']) && $depth != 0) {
        // Megamenu is still beta, there is a good chance much of this will be changed.
        if (!empty($settings['megamenu_exclude'])) {
          if (is_array($settings['megamenu_exclude'])) {
            $megamenu = (in_array($mlid, $settings['megamenu_exclude'])) ? 0 : $megamenu;
          }
          else {
            $megamenu = ($settings['megamenu_exclude'] == $mlid) ? 0 : $megamenu;
          }
          // Send the result to the sub-menu.
          $sfsettings['megamenu'] = $megamenu;
        }
        if ($megamenu == 1) {
          $megamenu_wrapper = ($menu_item['link']['depth'] == $settings['megamenu_depth']) ? TRUE : FALSE;
          $megamenu_column = ($menu_item['link']['depth'] == $settings['megamenu_depth'] + 1) ? TRUE : FALSE;
          $megamenu_content = ($menu_item['link']['depth'] >= $settings['megamenu_depth'] && $menu_item['link']['depth'] <= $settings['megamenu_levels']) ? TRUE : FALSE;
        }
        // Render the sub-menu.
        $var = array(
          'id' => $id,
          'menu' => $menu_item['below'],
          'depth' => $depth, 'trail' => $trail,
          'sfsettings' => $sfsettings
        );
        $children = theme('superfish_build', $var);
        // Check to see whether it should be displayed.
        $show_children = (($menu_item['link']['depth'] <= $depth || $depth == -1) && $children['content']) ? TRUE : FALSE;
        if ($show_children) {
          // Add item counter classes.
          if ($settings['itemcounter']) {
            $item_class[] = 'sf-total-children-' . $children['total_children'];
            $item_class[] = 'sf-parent-children-' . $children['parent_children'];
            $item_class[] = 'sf-single-children-' . $children['single_children'];
          }
          // More helper classes.
          $item_class[] = ($megamenu_column) ? 'sf-megamenu-column' : '';
          $item_class[] = $link_class[] = 'menuparent';
        }
        $parent_children++;
      }
      else {
        $item_class[] = 'sf-no-children';
        $single_children++;
      }

      $item_class = implode(' ', array_filter($item_class));

      if (isset($menu_item['link']['localized_options']['attributes']['class'])) {
        $link_class_current = $menu_item['link']['localized_options']['attributes']['class'];
        $link_class = array_merge($link_class_current, array_filter($link_class));
      }
      $menu_item['link']['localized_options']['attributes']['class'] = $link_class;

      $link_options['attributes'] = $menu_item['link']['localized_options']['attributes'];

      // Render the menu item.
      $output['content'] .= '<li id="menu-' . $mlid . '-' . $id . '"';
      $output['content'] .= ($item_class) ? ' class="' . trim($item_class) . '">' : '>';
      $output['content'] .= ($megamenu_column) ? '<div class="sf-megamenu-column">' : '';
      $output['content'] .= isset($whl[0]) ? $whl[0] : '';

      if (in_array('menuparent', $link_options['attributes']['class'])) {
        $output['content'] .= '<div class="' . implode(' ', $link_options['attributes']['class']) . '">' . $menu_item['link']['title'] . '</div>';
      }
      else {
        $output['content'] .= l($menu_item['link']['title'], $menu_item['link']['link_path'], $link_options);
      }

      $output['content'] .= isset($whl[1]) ? $whl[1] : '';
      $output['content'] .= ($megamenu_wrapper) ? '<ul class="sf-megamenu"><li class="sf-megamenu-wrapper ' . $item_class . '">' : '';
      $output['content'] .= ($show_children) ? (isset($wul[0]) ? $wul[0] : '') : '';
      $output['content'] .= ($show_children) ? (($megamenu_content) ? '<ol>' : '<ul>') : '';
      $output['content'] .= ($show_children) ? $children['content'] : '';
      $output['content'] .= ($show_children) ? (($megamenu_content) ? '</ol>' : '</ul>') : '';
      $output['content'] .= ($show_children) ? (isset($wul[1]) ? $wul[1] : '') : '';
      $output['content'] .= ($megamenu_wrapper) ? '</li></ul>' : '';
      $output['content'] .= ($megamenu_column) ? '</div>' : '';
      $output['content'] .= '</li>';
    }
  }
  $output['total_children'] = $total_children;
  $output['parent_children'] = $parent_children;
  $output['single_children'] = $single_children;
  return $output;
}

function maya_file_icon($vars) {
  $file = $vars['file'];
  $icon_directory = path_to_theme() . '/images/icons';
  $icon_url = file_icon_url($file, $icon_directory);
  $mime = check_plain($file->filemime);
  return '<img class="file-icon" width="48" height="48" alt="" title="' . $mime . '" src="' . $icon_url . '" />';
}

function maya_file_link($vars) {
  $file = $vars['file'];
  $icon_directory = $vars['icon_directory'];

  $url = file_create_url($file->uri);
  $icon = theme('file_icon', array('file' => $file, 'icon_directory' => $icon_directory));

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
      'class' => 'file',
    ),
    'html' => TRUE,
  );

  // Use the description as the link text if available.
  if (empty($file->description)) {
    $link_text = $file->filename;
  }
  else {
    $link_text = $file->description;
    $options['attributes']['title'] = check_plain($file->filename);
  }
  return l(t('<dl class="file"><dt>' . $icon . '</dt><dd>@link_text</dd></dl>', array('@link_text' => $link_text)), $url, $options);
}

function maya_fieldset($vars) {
  $element = $vars['element'];
  $collapsible = ($element['#collapsible']) ? ' collapsible' : '';
  element_set_attributes($element, array('id'));
  _form_set_class($element, array('form-wrapper'));

  $output = '<div class="fieldset' . $collapsible . '"><fieldset' . drupal_attributes($element['#attributes']) . '>';
  if (!empty($element['#title'])) {
    // Always wrap fieldset legends in a SPAN for CSS positioning.
    $output .= '<legend><span class="fieldset-legend">' . $element['#title'] . '</span></legend>';
  }
  $output .= '<div class="fieldset-wrapper">';
  if (!empty($element['#description'])) {
    $output .= '<div class="fieldset-description">' . $element['#description'] . '</div>';
  }
  $output .= $element['#children'];
  if (isset($element['#value'])) {
    $output .= $element['#value'];
  }
  $output .= '</div>';
  $output .= "</fieldset></div>\n";
  return $output;
}

function maya_preprocess_views_view_field(&$vars) {
  if ($vars['view']->display['default']->handler->view->style_plugin->definition['theme'] == 'views_view_unformatted') {
    $vars['theme_hook_suggestion'] = 'field-attachment';
  }

  $vars['output'] = $vars['field']->advanced_render($vars['row']);
}

function maya_apachesolr_search_snippets__file($vars) {
  $doc = $vars['doc'];
  $parent_entity_links = array();

  // Retrieve our parent entities. They have been saved as
  // a small serialized entity
  foreach ($doc->zm_parent_entity as $parent_entity_encoded) {
    $parent_entity = (object) drupal_json_decode($parent_entity_encoded);
    $parent_entity_uri = entity_uri($parent_entity->entity_type, $parent_entity);
    $parent_entity_uri['options']['absolute'] = TRUE;
    $parent_label = entity_label($parent_entity->entity_type, $parent_entity);
    $parent_entity_links[] = l($parent_label, $parent_entity_uri['path'], $parent_entity_uri['options']);
  }

  if (module_exists('file')) {
    $file_type = t('!icon', array('!icon' => theme('file_icon', array('file' => (object) array('filemime' => $doc->ss_filemime)))));
  }
  else {
    $file_type = t('@filemime', array('@filemime' => $doc->ss_filemime));
  }

  return '<div style="float:left;padding-right:5px;">' . $file_type . '</div><div style="margin-top:5px;">Anlage zu: ' . implode(', ', $parent_entity_links) . '</div><div style="clear:left;"></div>';
}
