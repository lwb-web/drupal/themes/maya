<?php

/**
 * @file
 *
 * settings
 */

/**
 *
 * Enter description here ...
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function maya_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['maya_site'] = array(
    '#type' => 'radios',
    '#title' => t('Maya as main-page'),
    '#options' => array(
      'main' => t('Main site'),
      'sub' => t('Sub site'),
    ),
    '#default_value' => theme_get_setting('maya_site', 'maya'),
    '#description' => t('Specify whether the site is the main site or a sub site.'),
    // Place this above the color scheme options.
    '#weight' => -2,
  );

  $node_types = node_type_get_types();

  $form['maya_container'] = array(
    '#type' => 'fieldset',
    '#title' => t('Maya settings'),
    '#description' => t('Show user pictures.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  foreach ($node_types as $type => $node_type) {
    $form['maya_container']['submitted_by_pic_' . $type] = array(
      '#type' => 'checkbox',
      '#title' => t($node_type->name),
      '#default_value' => theme_get_setting("submitted_by_pic_{$type}", 'maya'),
    );
  }
}
