<?php
/**
 * @file
 *
 * node.tpl.php
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php if (!$page): ?>node-list <?php endif; ?>	<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (isset($node->field_map[LANGUAGE_NONE])) : ?>
    <iframe id="process" style="border:none; width:100%; height:780px; overflow:auto;" src="<?php print $node->field_map[LANGUAGE_NONE][0]['original_url']; ?>"></iframe>

    <script type="text/javascript">
      /*var timer = window.setInterval("autoresize()", 1000);
       function autoresize() {
       jQuery("#process").height(jQuery("#process").contents().find("html").height());
       }*/
    </script>
  <?php endif; ?>

  <?php if (!empty($content['links'])): ?>
    <div class="links"><?php print render($content['links']); ?></div>
  <?php endif; ?>

  <?php if (!empty($content['comments'])): ?>
    <div class="comments"><?php print render($content['comments']); ?></div>
  <?php endif; ?>
</div>
