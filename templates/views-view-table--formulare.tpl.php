<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<?php
$cat = "";
$pos = 0;
?>

<table <?php if ($classes) {
  print 'class="' . $classes . '" ';
} ?><?php print $attributes; ?>>
  <?php if (!empty($title)) : ?>
    <caption><?php print $title; ?></caption>
      <?php endif; ?>
  <thead>
    <tr>
        <?php foreach ($header as $field => $label): ?>
          <?php if ($field != 'field_geschaeftsprozess'): ?>
          <th <?php if ($header_classes[$field]) {
          print 'class="' . $header_classes[$field] . '" ';
        } ?>>
    <?php print $label; ?>
          </th>
      <?php endif; ?>
    <?php endforeach; ?>
    </tr>
  </thead>
  <tbody>
        <?php foreach ($rows as $count => $row): ?>
          <?php if ($row['field_geschaeftsprozess'] != $cat): ?>
        <tr class="<?php print implode(' ', $row_classes[$pos]); ?> secondary">
        <?php $pos = ($pos == 0) ? 1 : 0; ?>
          <td colspan="100%">
        <?php print $row['field_geschaeftsprozess']; ?>
          </td>
        </tr>
          <?php $cat = $row['field_geschaeftsprozess']; ?>
        <?php endif; ?>
          <?php $pos = ($pos == 0) ? 1 : 0; ?>
      <tr class="<?php print implode(' ', $row_classes[$count]); ?>">
        <?php foreach ($row as $field => $content): ?>
          <?php if ($field != 'field_geschaeftsprozess'): ?>
            <td <?php if ($field_classes[$field][$count]) {
        print 'class="' . $field_classes[$field][$count] . '" ';
      } ?><?php print drupal_attributes($field_attributes[$field][$count]); ?>>
      <?php print $content; ?>
            </td>
    <?php endif; ?>
  <?php endforeach; ?>
      </tr>
<?php endforeach; ?>
  </tbody>
</table>
