<?php

/**
 * @file
 *
 * views-view-field--field-attachment.tpl.php
 */
foreach ($row->_field_data as $entity_def) {
  $entity = $entity_def['entity'];
  $entity_type = $entity_def['entity_type'];

  if (isset($entity->field_attachment)) {
    $field = field_read_field('field_attachment');
    $languages = field_available_languages($entity_type, $field);

    foreach ($languages as $language) {
      if (isset($entity->field_attachment[$language])) {
        $files = $entity->field_attachment[$language];
        $br = (count($files) > 1) ? '<br />' : '';

        foreach ($files as $file_array) {
          $file = (object) $file_array;
          $icon_url = file_icon_url($file);
          $icon = '<img class="file-icon" alt="" title="" src="' . $icon_url . '" />';
          $url = file_create_url($file->uri);

          // Set options as per anchor format described at
          // http://microformats.org/wiki/file-format-examples
          $options = array(
            'attributes' => array(
              'type' => $file->filemime . '; length=' . $file->filesize,
            ),
          );

          // Use the description as the link text if available.
          if (empty($file->description)) {
            $link_text = $file->filename;
          }
          else {
            $link_text = $file->description;
            $options['attributes']['title'] = check_plain($file->filename);
          }

          print '<span class="file">' . $icon . ' ' . l($link_text, $url, $options) . '</span>' . $br;
        }
      }
    }
  }
}
?>