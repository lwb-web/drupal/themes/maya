<?php
/**
 * @file
 *
 * file for comment template
 */
?>
<div class="comment-node <?php print $classes; ?>">
  <div class="comment-info">
    <?php if ($picture) print $picture; ?>
    <?php print $submitted; ?>
    <div class="comment-date"><?php print $created; ?></div>
  </div>

  <div class="comment-inner clear-block">
    <!-- <?php print render($title_prefix); ?>
      <h3<?php print $title_attributes; ?>><?php if ($new): ?><span class="new"><?php print $new; ?>&nbsp;-&nbsp;</span><?php endif; ?><?php print $title; ?></h3>
    <?php print render($title_suffix); ?> -->

    <div class="clear-block">
      <?php
      hide($content['links']);
      print render($content);
      ?>
    </div>
  </div>

  <?php if (!empty($content['links'])): ?>
    <div class="links"><?php print render($content['links']); ?></div>
  <?php endif; ?>
</div> <!-- /posting -->