<?php
/**
 * @file
 *
 * page--dashboard.tpl.php
 */
?>
<div id="page-wrapper">
  <header class="clearfix">
    <?php if ($logo): ?>
      <a
        href="<?php print (theme_get_setting('maya_site', 'maya') == 'main') ? $front_page : 'http://web1.lwb.local/' ; ?>"
        title="<?php print t('Home'); ?>" rel="home"><img
          src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"
          id="logo" width="148" height="45" /> </a>
      <?php endif; ?>

    <div id="header-region">
      <div class="region-header">
        <?php if ($secondary_menu): ?>
          <div id="secondary-menu" class="navigation">
            <?php
            print theme('links__system_secondary_menu', array(
              'links' => $secondary_menu,
              'attributes' => array(
                'id' => 'secondary-menu-links',
                'class' => array('links', 'inline', 'clearfix'),
              ),
              'heading' => array(
                'text' => t('Secondary menu'),
                'level' => 'h2',
                'class' => array('element-invisible'),
              ),
            ));
            ?>
          </div>
          <!-- /#secondary-menu -->
        <?php endif; ?>
      </div>

      <?php if ($page['header']): ?>
        <?php print render($page['header']); ?>
      <?php endif; ?>
    </div>
    <!-- /header-region -->

    <?php if (theme_get_setting('maya_site', 'maya') == 'sub'): ?>
      <div id="site-description">
        <a href="<?php print $front_page; ?>"
           title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?>
        </a>
      </div>
    <?php endif; ?>
  </header>
  <!-- /header -->

  <?php if ($page['menu']): ?>
    <div id="menu" class="clearfix">
      <?php print render($page['menu']); ?>
      <?php if ($page['menu'] || $secondary_menu): ?>
        <button class="menu-toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      <?php endif; ?>
    </div>
  <?php else: ?>
    <div id="empty-colorline"></div>
  <?php endif; ?>

  <div id="secondary-colorline" class="clearfix">
    <div class="my-color"></div>
  </div>
  <!-- /secondary-colorline -->

  <div ng-app="maya" id="container" class="clearfix">
    <div id="main" class="column">
      <div id="squeeze" class="clearfix">
        <div id="maya-content" class="maya-content">
          <div class="front-left front-field">
            <div ui-view="links"></div>
          </div>
          <?php if ($page['front_top']): ?>
            <div class="front-top front-field">
              <?php print render($page['front_top']); ?>
            </div>
          <?php endif; ?>
          <div class="front-right front-field">
            <div ui-view="info"></div>
          </div>
          <div class="front-center">
            <?php print render($page['help']); ?>
            <?php print $messages; ?>
            <div ui-view="dash"></div>
          </div>&nbsp;
        </div>
      </div>
    </div>
  </div>
  <!-- /content /squeeze /main /container -->

  <footer>
    <div class="footer-wrapper">
      <?php if ($page['footer']): ?>
        <?php print render($page['footer']); ?>
      <?php endif; ?>
    </div>
    <div class="stop"></div>
  </footer>
  <!-- /#footer-wrapper /#footer -->
</div>
<!-- /page -->
