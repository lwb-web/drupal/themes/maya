<?php
/**
 * @file
 *
 * node.tpl.php
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php if (!$page): ?>node-list <?php endif; ?>	<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$page): ?>
    <div class="node-header">
    <?php endif; ?>

    <?php print render($title_prefix); ?>
    <?php if (!$page): ?>
      <h2 class="title"<?php print $title_attributes; ?>>
        <a href="<?php print $node_url; ?>" title="<?php print $title ?>"><?php print $title; ?></a>
      </h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>

    <?php if ($display_submitted && $page): ?>
      <div class="node-info">
        <?php print $submitted; ?>
      </div>
    <?php endif; ?>
    <?php if (!$page): ?>
    </div>
  <?php endif; ?>
  <div class="clearfix"<?php print $content_attributes; ?>>
    <?php
    hide($content['comments']);
    hide($content['links']);
    print render($content);
    ?>
  </div>
  <?php if (!empty($content['links'])): ?>
    <div class="links"><?php print render($content['links']); ?></div>
  <?php endif; ?>

  <?php if (!empty($content['comments'])): ?>
    <div class="comments"><?php print render($content['comments']); ?></div>
  <?php endif; ?>
</div>
